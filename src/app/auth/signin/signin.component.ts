import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

import { HttpHandlerService } from '../../services/http-handler.service';

import {MatSnackBar} from '@angular/material/snack-bar';

import { LoadingService } from '../../services/loading/loading.service'

interface Params {
  userID: string;
  password: string;
}

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
})
export class SigninComponent implements OnInit {

  params = {email: '',  password: ''  };
  response :any;

  constructor( public httpHandler :HttpHandlerService , private loadingService : LoadingService , public router :Router , private _snackBar: MatSnackBar ) {}

  ngOnInit(): void {
  }

  // API Returns 400 code for inocorrect username and password should return 200
  // 400 for not meeting required parameter requirement
  signin() {
    this.loadingService.show();
    this.httpHandler.httpHandler('users/authenticate',this.params)
      .subscribe(data => {
        this.loadingService.hide();
        if( data.status == 'success'){
          localStorage.setItem('token',data.data['token']);
          this.router.navigateByUrl('/dashboard');
        }
      },
      error => {
        this.response = error.error;
        this.loadingService.hide();
        this._snackBar.open('Incorrect Username or Password','Failure', {
          duration: 2000,
        });
      }
    );
  }

}
