import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

import { HttpHandlerService } from '../../services/http-handler.service';

// import { LoadingService } from '../../services/loading/loading.service'

interface Params {
  userID: string;
  password: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  params = { name : '' , email: '',  password: '' };
  response :any;

  confirmPassword = '';

  constructor( public httpHandler :HttpHandlerService, public router :Router ) {}

  ngOnInit(): void {
  }

  signup() {
    this.httpHandler.httpHandler('users/register',this.params)
      .subscribe(data => {
        if( data.status == 'success'){
          alert('fuiyo');
          // localStorage.setItem('token',data.data['token']);
          // this.router.navigateByUrl('/dashboard');
        }
      },
      error => {
        console.log(error);
        this.response = error.error;
        // alert('---------------haiya');
      }
    );
  }

}
