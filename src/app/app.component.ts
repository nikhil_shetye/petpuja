import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { v4 as uuid } from 'uuid';

// import { AddRestaurantAction } from './store/actions/browse.actions';
// import { Restaurant } from './store/models/restaurants.models';
// import { AppState } from './store/models/app-state.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Junky';

  // restaurants : Observable<Array<Restaurant>>;
  // newRestaurant : Restaurant = { _id: '', name: '' }

  constructor() { }

  ngOnInit() {
    // this.restaurants = this.store.select(store => store.browse);
  }

  // addItem() {
  //   this.newRestaurant._id = uuid();
  //   this.store.dispatch(new AddRestaurantAction(this.newRestaurant));
  //   this.newRestaurant = { _id: '', name: '' };
  // }

}
