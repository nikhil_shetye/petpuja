import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FlexLayoutModule } from '@angular/flex-layout';

import { DashboardRoutingModule } from './dashboard-routing.module';

import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import { DashboardComponent } from './dashboard.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { SaveRestaurantComponent } from './saveRestaurant/save-restaurant.component';
import { DeleteRestaurantComponent } from './helperComponents/deleteRestaurant/delete-restaurant.component';
import { PickTimeRangeComponent } from './helperComponents/pickTimeRange/pick-time-range.component';

import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

import { BrowseReducer } from '../store/reducers/browse.reducer';
import { BrowseEffects } from '../store/effects/browse.effects';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [ 
    DashboardComponent,
    DeleteRestaurantComponent,
    PickTimeRangeComponent,
    RestaurantComponent,
    SaveRestaurantComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    NgxMaterialTimepickerModule,
    StoreModule.forFeature( 'browse' ,BrowseReducer ),
    EffectsModule.forFeature( [ BrowseEffects ] )
  ]
})
export class DashboardModule { }
