import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { SaveRestaurantComponent } from './saveRestaurant/save-restaurant.component';

const routes: Routes = [
  { path : '' , component : DashboardComponent ,
    children : [
      { path : '', component: RestaurantComponent },
      { path : 'restaurants', component: RestaurantComponent },
      { path : 'saverestaurant/:id', component: SaveRestaurantComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
