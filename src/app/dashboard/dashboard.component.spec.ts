import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponenet } from './dashboard.component';

describe('DashboardComponenet', () => {
  let component: DashboardComponenet;
  let fixture: ComponentFixture<DashboardComponenet>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponenet ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponenet);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
