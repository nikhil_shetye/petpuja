import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';


import { AppState } from '../store/models/app-state.model';

import { LoadRestaurantAction,LoadUserAction } from '../store/actions/browse.actions';
// import { SaveRestaurantComponent } from './helperComponents/saveRestaurant/save-restaurant.component';

interface Params {
  userID: string;
  password: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  response: Response;

  constructor( private _snackBar: MatSnackBar , public router: Router  , private store: Store<AppState> , public dialog : MatDialog ) {}

  ngOnInit(): void {
    this.store.dispatch(new LoadRestaurantAction());
    this.store.dispatch(new LoadUserAction());
    setInterval(() => {
      this.store.dispatch(new LoadRestaurantAction());},30000
    )
  }

  saveRestaurant( restaurant ){
    this.router.navigateByUrl('/dashboard/saverestaurant/0');
  }

  // saveRestaurant(restaurant){
  //   const dialogRef = this.dialog.open( SaveRestaurantComponent ,  {
  //     width : '300px',
  //     data : { restaurant : restaurant }
  //   } );
  //   dialogRef.afterClosed().subscribe(result => {
  //     // if(result)
  //     //   this.loadRestaurant();
  //   });
  // }

  signOut(){
    localStorage.removeItem('token');
    this.router.navigateByUrl('/');
    this._snackBar.open('Signed Out of Session','Success', {
      duration: 2000,
    });
  }
}
