import { WeekDay } from '@angular/common';
import { Component , Inject, OnInit } from '@angular/core';

import { ActivatedRoute , Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { PickTimeRangeComponent } from '../helperComponents/pickTimeRange/pick-time-range.component';

import { HttpHandlerService } from '../../services/http-handler.service';
import { LoadingService } from '../../services/loading/loading.service'
import { flatMap } from 'rxjs/operators';

interface Restaurant {
  restaurant : any;
}

@Component({
  selector: 'dialog-save-restaurant',
  templateUrl : './save-restaurant.component.html',
  styleUrls: ['./save-restaurant.component.sass']
})
export class SaveRestaurantComponent implements OnInit {

  saveRestaurantParams :Restaurant;
  saveRestaurantFields = [];
  hours = [];
  preparedHours = [];

  sub: any;

  weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

  emptyArea = {'Sunday':'','Monday':'','Tuesday':'','Wednesday':'','Thursday':'','Friday':'','Saturday':''};

  constructor( private activatedRoute : ActivatedRoute , public dialog : MatDialog ,  private httpHandler : HttpHandlerService , private loadingService : LoadingService , public router: Router ) {
    this.saveRestaurantFields = [
      { type : 'text' , placeholder: '_id' , model : '' , hide : true },
      { type : 'text' , placeholder: 'Name' , model : '' },
      { type : 'text' , placeholder: 'Category' , model : '' },
      { type : 'textarea' , placeholder: 'About' , model : '' }
    ]
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.paramMap.subscribe(params => { 
      var currenRestaurant = params.get('id');
      if( currenRestaurant != '0' ){
        this.loadingService.show()
        this.httpHandler.httpAuthorizedGetHandler('restaurant',currenRestaurant)
          .subscribe(data => {
            this.adjusterReverse(data.data['movies'],this.saveRestaurantFields);
            this.minimizer(data.data['movies'].hours);
            this.loadingService.hide();
          },
          error => {
            localStorage.removeItem('token');
            this.router.navigateByUrl('/');
            this.loadingService.hide()
          }
        );
      } else
        this.minimizer(this.emptyArea);
    });
  }

  saveRestaurant(){
    this.loadingService.show()

    let params = this.adjuster(this.saveRestaurantFields);
    params['hours'] = this.prepareSlots();
    if( params['_id'] == ''  ){
      this.httpHandler.httpAuthorizedPostHandler('restaurant',params)
        .subscribe(data => {
          this.loadingService.hide();
          this.router.navigateByUrl('/dashboard');
        },
        error => {
          this.loadingService.hide();
          console.log(error);
          alert('some error')
        }
      );
    } else {
      this.httpHandler.httpAuthorizedPutHandler('restaurant',params)
        .subscribe(data => {
          this.loadingService.hide();
          this.router.navigateByUrl('/dashboard');
        },
        error => {
          this.loadingService.hide();
          console.log(error);
          alert('some error')
        }
      );
    } 
  }

  addSlot(hour){
    hour.slots.push([{ hour:0,minute:0 , time:('00:00') },{ hour:0,minute:0 , time:('00:00') }])
  }

  prepareSlots(){
    var hours = {};
    this.hours.forEach( hour => {
      var hourRoster = '';
      hour.slots.forEach( slot => {
        var time = slot[0].time.trim().split(':');
        var time1 = (Number(time[0])*60)+Number(time[1]);
        time = slot[1].time.trim().split(':');
        var time2 = (Number(time[0])*60)+Number(time[1]);
        hourRoster = hourRoster + time1.toString() + '-' + time2.toString() + ',';
      });
      hours[hour.day] = hourRoster.slice(0, -1);
    });
    console.log(hours);
    return hours;
  }

  adjusterReverse( params,fields ){
    fields.forEach(element => {
      if( params.hasOwnProperty(element.placeholder.toLowerCase()) ){
        element.model = params[element.placeholder.toLowerCase()];
      }
    });
  }

  adjuster(fields){
    var params = {};
    fields.forEach(element => {
      params[element.placeholder.toLowerCase()] = element.model;
    });
    return params;
  }

  test(slot){
    var time = slot.time.trim().split(':');
    slot.hours = time[0];
    slot.minutes = time[1];
  }

  minimizer(params){
    if(params != undefined){
      this.weekday.forEach(element => {
        var timeSlots = [];
        if( params.hasOwnProperty(element) ){
          var slots = params[element].trim().split(',');
          if(slots != ''){
            if( slots.length > 0 ){
              slots.forEach(slot => {
                var bracket = [];
                if(slot != ''){
                  var time = slot.split('-');
                  time.forEach(element => {
                    if(!isNaN(Number(element))){
                      var hours = Math.floor(element/60);
                      var minutes = Math.floor(element%60);
                      bracket.push( { hour:hours,minute:minutes , time:( ( ((hours.toString().length > 1) ? hours.toString() : '0'+hours) ) + ':' + ( ((minutes.toString().length > 1) ? minutes.toString() : '0'+minutes) ) ) });
                    } else
                    bracket.push( { hour:0,minute:0 , time:'00:00' });
                  });
                } else
                timeSlots.push(bracket);
              });
            }
          }        
        }
        this.hours.push({ day : element , slots : timeSlots })
      });
    }
  }

}
