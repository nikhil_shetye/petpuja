import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Router,NavigationExtras } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

// import { SaveRestaurantComponent } from '../helperComponents/saveRestaurant/save-restaurant.component';
import { DeleteRestaurantComponent } from '../helperComponents/deleteRestaurant/delete-restaurant.component';

import { LoadingService } from '../../services/loading/loading.service';
import { HttpHandlerService } from '../../services/http-handler.service';

import { Restaurant } from '../../store/models/restaurants.models';
import { AppState } from '../../store/models/app-state.model';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.sass']
})
export class RestaurantComponent implements OnInit {
  params = { email: '',  password: '' };
  response :any;

  restaurants :any;
  users : any;

  restaurantsNeo : Observable<Array<Restaurant>>;
  rawRestaurant : any;

  state :any;

  weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

  statuses = [
    { status : 'Unknown' , class : '' },
    { status : 'Open' , class : 'accent' },
    { status : 'Closed' , class : 'warn' }
  ];

  restaurantDisplayedColumns :string[] = [ 'name' , 'status' ,'category' , 'action' ];
  userDisplayedColumns :string[] = [ 'name' , 'email' ];

  constructor( public httpHandler : HttpHandlerService , public dialog : MatDialog , public router: Router , private store: Store<AppState> ) {}

  ngOnInit(): void {
    this.store.select(store => store.browse).subscribe(
      value => {
        this.state = value;
        if(this.state['error'] != undefined)
          this.router.navigateByUrl('/');
        this.restaurants = new MatTableDataSource(this.reformRestaurant([...this.state['restaurants']]));
        this.users = new MatTableDataSource(this.reformRestaurant([...this.state['users']]));
      }
    );
  }
  
  saveRestaurant( restaurant ){
    this.router.navigateByUrl('/dashboard/saverestaurant/'+restaurant['_id']);
  }

  reformRestaurant(restaurants) {
    var d = new Date();
    var n = d.getDay();
    var minutes = ( d.getHours()*60 + d.getMinutes() );
    var reformedRestaurants = [];
    // console.log('restaurant length',restaurants,restaurants.length);
    if(restaurants.length > 0){
      restaurants.forEach(element => {
        var status = 0;
        if(element.hasOwnProperty('hours')){
          if(element.hours.hasOwnProperty(this.weekday[n])){
            var status = 2;
            var slots = element.hours[this.weekday[n]].split(',');
            slots.forEach(elementSlots => {
              if( elementSlots.status != 1){
                var bracket = elementSlots.split('-');
                if( bracket[0] < minutes && minutes < bracket[1] ){
                  status = 1;
                }
              }
              
            });
          }
        }
        reformedRestaurants.push({...element,status : status});
      });
    }
    return reformedRestaurants;
  }

  deleteRestaurant(restaurant){
    const dialogRef = this.dialog.open( DeleteRestaurantComponent ,  {
      width : '300px',
      data : { restaurant : restaurant }
    } );
    dialogRef.afterClosed().subscribe(result => {
      // if(result)
      //   this.loadRestaurant();
    });
  }

  applyFilterRestaurant( filter :string ){
    this.restaurants.filter = filter.trim().toLowerCase();
  }

  applyFilterUser( filter :string ){
    this.users.filter = filter.trim().toLowerCase();
  }
}