import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { HttpHandlerService } from '../../../services/http-handler.service';
import { LoadingService } from '../../../services/loading/loading.service'

interface Restaurant {
  restaurant : any;
}

@Component({
  selector: 'dialog-pick-time-range',
  templateUrl : './pick-time-range.component.html',
  styleUrls: ['./pick-time-range.component.sass']
})
export class PickTimeRangeComponent {

  constructor( @Inject(MAT_DIALOG_DATA) public data: Restaurant , private httpHandler : HttpHandlerService , private dialogRef: MatDialogRef<PickTimeRangeComponent> , private loadingService : LoadingService ) {    
    console.log('restaurant',data);
  }

  ngOnInit(): void {}

  adjuster(fields){
    var params = {};
    fields.forEach(element => {
      params[element.placeholder.toLowerCase()] = element.model;
    });
    return params;
  }

}
