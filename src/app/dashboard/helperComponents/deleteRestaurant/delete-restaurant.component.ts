import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { HttpHandlerService } from '../../../services/http-handler.service';
import { LoadingService } from '../../../services/loading/loading.service'

interface Restaurant {
  restaurant : any;
}

@Component({
  selector: 'dialog-delete-restaurant',
  templateUrl : './delete-restaurant.component.html',
  styleUrls: ['./delete-restaurant.component.sass']
})
export class DeleteRestaurantComponent {

  constructor( @Inject(MAT_DIALOG_DATA) public data: Restaurant , private httpHandler : HttpHandlerService , private dialogRef: MatDialogRef<DeleteRestaurantComponent> , private loadingService : LoadingService ) {    
    console.log('restaurant',data);
  }

  ngOnInit(): void {}

  deleteRestaurant(){
    this.loadingService.show();
    this.httpHandler.httpAuthorizedDeleteHandler('restaurant',this.data.restaurant['_id'])
      .subscribe(data => {
        this.loadingService.hide();
        this.dialogRef.close(true);
        },
        error => {
          this.loadingService.hide();
          console.log(error);
        }
      );
  }

  adjuster(fields){
    var params = {};
    fields.forEach(element => {
      params[element.placeholder.toLowerCase()] = element.model;
    });
    return params;
  }

}
