import { Injectable } from '@angular/core';

import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loadingStatus: any;

  constructor() {
    this.loadingStatus = new Subject();
  }

  show(){
    this.loadingStatus.next(true);
  }

  hide(){
    this.loadingStatus.next(false);
  }
  
}
