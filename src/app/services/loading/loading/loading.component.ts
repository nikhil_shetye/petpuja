import { Component, OnInit } from '@angular/core';

import { LoadingService } from '../loading.service'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.sass']
})
export class LoadingComponent implements OnInit {

  screenWidth: number;
  screenHeight: number;
  loadingScreenOpacity: number;
  displaySetting: string;

  loading: boolean;

  loadingSubsription: Subscription;

  constructor(private loadingService: LoadingService) { }

  ngOnInit(): void {
    this.loading = false;
    this.screenHeight = screen.height;
    this.screenWidth = screen.width;

    this.loadingSubsription = this.loadingService.loadingStatus.subscribe(
      value =>{
        this.loading = value;
      }
    );
  }



}
