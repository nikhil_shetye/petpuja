import { TestBed } from '@angular/core/testing';

import { ConstantineService } from './constantine.service';

describe('ConstantineService', () => {
  let service: ConstantineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConstantineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
