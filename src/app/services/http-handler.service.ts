import { Injectable } from '@angular/core';

import { HttpClient,HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { Router } from '@angular/router';

import { ConstantineService } from './constantine.service';
import { LoadingService } from './loading/loading.service';

interface Message {
  status: string;
  message: any[any];
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpHandlerService {
  baseUrl: string;

  constructor(public httpClient: HttpClient, public router: Router ,private loadingService: LoadingService,private constantineService: ConstantineService ) {
    this.baseUrl = 'http'+constantineService.apiBaseUrl;
  }

  httpAuthorizedGetHandler(api,params): Observable<Message> {    
    const httpOptions = {
      headers: new HttpHeaders().set('x-access-token',localStorage.getItem('token'))
    };

    return this.httpClient.get<Message>(this.baseUrl+api+((params == 0) ? '' : '/'+params), httpOptions)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  // httpAuthorizedHandler(api,params): Observable<Message> {
  //   const httpOptions = {
  //     headers: new HttpHeaders().set('Authorization', 'Bearer '+ sessionStorage.getItem('token'))
  //   };
  //   return this.httpClient.post<Message>(this.baseUrl+api, params, httpOptions)
  //   .pipe(
  //       catchError(this.httpAuthorizedErrorHandler),
  //   );
  // }

  httpAuthorizedTokenHandler(api,params,token): Observable<Message> {
    const httpOptions = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+ token)
    };
    
    return this.httpClient.post<Message>(this.baseUrl+api, params, httpOptions)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  httpAuthorizedPostHandler(api,params): Observable<Message> {
    const httpOptions = {
      headers: new HttpHeaders().set('x-access-token',localStorage.getItem('token'))
    };
    return this.httpClient.post<Message>(this.baseUrl+api, params, httpOptions)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  httpAuthorizedPutHandler(api,params): Observable<Message> {
    const httpOptions = {
      headers: new HttpHeaders().set('x-access-token',localStorage.getItem('token'))
    };
    
    return this.httpClient.put<Message>(this.baseUrl+api+'/'+params['_id'], params, httpOptions)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  httpAuthorizedDeleteHandler(api,param): Observable<Message> {
    const httpOptions = {
      headers: new HttpHeaders().set('x-access-token',localStorage.getItem('token'))
    };

    return this.httpClient.delete<Message>(this.baseUrl+api+'/'+param, httpOptions)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  httpHandler(api,params): Observable<Message> {
    return this.httpClient.post<Message>(this.baseUrl+api, params)
    .pipe(
        catchError(this.httpAuthorizedErrorHandler),
    );
  }

  errorCatcher(error){
    this.loadingService.hide();
    switch(error.status){
      case 401 :
        this.router.navigateByUrl('/login');
        // this.displayToastService.displayToast('danger_Failure_Token has Expired.');
        break;
      case 404 :
        // this.displayToastService.displayToast('danger_Apologies_Having Problem Connecting.');
      case 405 :
        // this.displayToastService.displayToast('danger_Apologies_Some Unknown error occured.');
    }
  }

  httpAuthorizedErrorHandler(error: HttpErrorResponse){
    return throwError(error);
  }

}
