import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantineService {

  public apiBaseUrl: string;

  constructor() {
    // this.apiBaseUrl = "s://api.alphaquad.in";
    this.apiBaseUrl = "://akup96.deta.dev/";
  }
}
