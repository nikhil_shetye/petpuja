import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FulfillingSquareSpinnerModule } from 'angular-epic-spinners';

import { LoadingComponent } from './services/loading/loading/loading.component';
import { LoadingService } from './services/loading/loading.service';

import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// import { BrowseReducer } from './store/reducers/browse.reducer';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent
  ],
  imports: [
    AppRoutingModule,
    AuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    DashboardModule,
    FormsModule,
    FulfillingSquareSpinnerModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot()
    // StoreModule.forRoot({
    //   browse : BrowseReducer
    // })
  ],
  providers: [ LoadingService ],
  bootstrap: [ AppComponent , [ LoadingService ]]
})
export class AppModule { }
