import { Action } from '@ngrx/store';
import { Restaurant } from '../models/restaurants.models';

export enum BrowseActionTypes {
    LOAD_RESTAURANT = '[Browse] Load Restaurant',
    LOAD_RESTAURANT_SUCCESS = '[Browse] Load Restaurant Success',
    LOAD_RESTAURANT_FAILURE = '[Browse] Load Restaurant Failure',
    LOAD_USER = '[Browse] Load User',
    LOAD_USER_SUCCESS = '[Browse] Load User Success',
    LOAD_USER_FAILURE = '[Browse] Load User Failure',
    ADD_USER = '[Browse] Add User',
}

///////////////////////////////////////////////////////////

// export class AddRestaurantAction implements Action {
//     readonly type = BrowseActionTypes.ADD_RESTAURANT
  
//     constructor(public payload: Restaurant) { }
// }

export class LoadRestaurantAction implements Action {
    readonly type = BrowseActionTypes.LOAD_RESTAURANT
}

export class LoadRestaurantSuccessAction implements Action {
    readonly type = BrowseActionTypes.LOAD_RESTAURANT_SUCCESS

    constructor(public payload: Array<Restaurant>) { }
}

export class LoadRestaurantFailureAction implements Action {
    readonly type = BrowseActionTypes.LOAD_RESTAURANT_FAILURE

    constructor(public payload: Error) { }
}

///////////////////////////////////////////////////////////

export class LoadUserAction implements Action {
    readonly type = BrowseActionTypes.LOAD_USER
}

export class LoadUserSuccessAction implements Action {
    readonly type = BrowseActionTypes.LOAD_USER_SUCCESS

    constructor(public payload: Array<Restaurant>) { }
}

export class LoadUserFailureAction implements Action {
    readonly type = BrowseActionTypes.LOAD_USER_FAILURE

    constructor(public payload: Error) { }
}

export type RestaurantAction = LoadRestaurantAction|LoadRestaurantSuccessAction|LoadRestaurantFailureAction|LoadUserAction|LoadUserSuccessAction|LoadUserFailureAction;