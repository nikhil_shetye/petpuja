import { Restaurant } from "../models/restaurants.models";
import { User } from "../models/users.models";
import { RestaurantAction,BrowseActionTypes } from "../actions/browse.actions";

import { Router,NavigationExtras } from '@angular/router';

export interface BrowseState {
  restaurants : Restaurant[],
  users : User[],
  loading : boolean,
  error : Error
}



const initialState: BrowseState = {
  restaurants : [],
  users : [],
  loading : false,
  error : undefined
} ;

// TO:DO need to implemeny ngrx entity
export function BrowseReducer(state: BrowseState = initialState, action: RestaurantAction ,router : Router) {
    switch (action.type) {
      case BrowseActionTypes.LOAD_RESTAURANT:
        return { ...state , loading : true };
      case BrowseActionTypes.LOAD_RESTAURANT_SUCCESS:
        console.log('-------------------',action.payload);
        return { ...state , restaurants : action.payload };
      case BrowseActionTypes.LOAD_RESTAURANT_FAILURE:
        return { ...state , loading : false , error : action.payload };
      case BrowseActionTypes.LOAD_USER:
          return { ...state , loading : true };
      case BrowseActionTypes.LOAD_USER_SUCCESS:
          return { ...state , users : action.payload };
      case BrowseActionTypes.LOAD_USER_FAILURE:
          return { ...state , loading : false };
      default:
        return state;
    }
}
