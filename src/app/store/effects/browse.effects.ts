import { Injectable } from '@angular/core';
import { Actions , createEffect , ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';

import { BrowseActionTypes, LoadRestaurantAction , LoadRestaurantFailureAction, LoadRestaurantSuccessAction , LoadUserAction, LoadUserSuccessAction,LoadUserFailureAction } from '../actions/browse.actions'
import { of } from 'rxjs';
import { HttpHandlerService } from '../../services/http-handler.service';

@Injectable()
export class BrowseEffects {

    loadRestaurants$ = createEffect( () => this.actions$.pipe(
            ofType<LoadRestaurantAction>(BrowseActionTypes.LOAD_RESTAURANT),
            mergeMap( () => this.httpHandlerService.httpAuthorizedGetHandler('restaurant','').pipe(
                map(data => {
                    if(data['status'] == 'error'){
                        return new LoadRestaurantFailureAction(Error(data['status']))
                    } else
                        return new LoadRestaurantSuccessAction(data['restaurants'])
                    
                  }),
                  catchError(error => of(new LoadRestaurantFailureAction(error)))
                )
            )
        )
    )

    loadUsers$ = createEffect( () => this.actions$.pipe(
            ofType<LoadUserAction>(BrowseActionTypes.LOAD_USER),
            mergeMap( () => this.httpHandlerService.httpAuthorizedGetHandler('users','').pipe(
                map(data => {
                    return new LoadUserSuccessAction(data['users'])
                }),
                catchError(error => of(new LoadUserFailureAction(error)))
                )
            )
        )
    )

    constructor(
        private actions$: Actions,
        private httpHandlerService : HttpHandlerService
    ) {}
}