export interface Restaurant{
    _id : string;
    name : string;
    about : string;
    category : string;
    hours : {};
    status : number;
}