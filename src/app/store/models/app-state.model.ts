import { Restaurant } from './restaurants.models';

export interface AppState {
  readonly browse: Array<Restaurant>
}